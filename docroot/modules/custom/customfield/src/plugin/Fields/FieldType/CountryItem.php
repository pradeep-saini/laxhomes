<?php


namespace Drupal\customfield\Plugin\Field\FieldType;

use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_example_rgb' field type.
 *
 * @FieldType(
 *   id = "country",
 *   label = @Translation("Country"),
 *   default_widget = "country_default",
 *   default_formatter = "country_default"
 * )
 */
class CounryItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'deccription' => t('Country'),
          'length' => 2,
          'not null' => FALSE,
        ],
      ],
      'indexes' => [
      	'value' => ['value']
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  // public function isEmpty() {
  //   $value = $this->get('value')->getValue();
  //   return $value === NULL || $value === '';
  // }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
        ->setLabel(t('Country'));

    return $properties;
  }

}
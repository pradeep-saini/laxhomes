<?php

namespace Drupal\customfield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "country",
 *   label = @Translation("Country default"),
 *   field_types = {
 *     "country"
 *   }
 * )
 */
class CountryDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  // public function settingsSummary() {
  //   $summary = [];
  //   $settings = $this->getSettings();

  //   $summary[] = t('Displays the random string.');

  //   return $summary;
  // }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $counteries = \Drupal::service('country_manager')->getList();
    foreach ($items as $delta => $item) {
      // Render each element as markup.
    	if(isset($counteries[$item->value])){
    		$elements[$delta] = [
	        '#type' => 'markup',
	        '#markup' => '<h1>'.$counteries[$item->value].'</h1>';
	      ];
    	}
    }

    return $elements;
  }

}
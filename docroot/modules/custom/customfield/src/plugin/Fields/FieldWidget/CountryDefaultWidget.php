<?php 


namespace Drupal\customfield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "country_default",
 *   label = @Translation("Country default"),
 *   field_types = {
 *     "country"
 *   }
 * )
 */
class CountryDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $elements = [];

    $elements['value'] = [
      '#type' => 'select',
      '#options' => \Drupal::service('country_manager')->getList(),
      '#titel' => t('select country')
    ];

    return ['value' => $elements];
  }

  /**
   * Validate the color text field.
   */
  // public static function validate($element, FormStateInterface $form_state) {
  //   $value = $element['#value'];
  //   if (strlen($value) == 0) {
  //     $form_state->setValueForElement($element, '');
  //     return;
  //   }
  //   if (!preg_match('/^#([a-f0-9]{6})$/iD', strtolower($value))) {
  //     $form_state->setError($element, t("Color must be a 6-digit hexadecimal value, suitable for CSS."));
  //   }
  // }

}
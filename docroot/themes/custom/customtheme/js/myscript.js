 (function($, Drupal) {
	 Drupal.behaviors.window_load = {
	   attach: function(context, settings) {  

			$(document).ready(function() {
				 $('.navigation_tab_communities ul li a').click(function(){
					var tab_id = $(this).attr('data-tab');
					$('.navigation_tab_communities ul li a').removeClass('active_class');
					$('.community_details').removeClass('active_class');
					$(this).addClass('active_class');
					$("#"+tab_id).addClass('active_class');
				});
			});
		}
	}
})(jQuery, Drupal);
